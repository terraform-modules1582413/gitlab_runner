variable "argocd_namespace" {
  type    = string
  default = "argocd"
}
variable "argocd_project" {
  type    = string
  default = "default"
}
variable "argocd_destination_cluster" {
  type    = string
  default = "https://kubernetes.default.svc"
}

variable "gitlab_runner_repo_url" {
  type = string
}
variable "gitlab_runner_namespace" {
  type = string
}
variable "gitlab_runner_token" {
  type      = string
  sensitive = true
}
