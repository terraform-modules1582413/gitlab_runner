resource "kubernetes_secret" "argocd_repository" {
  metadata {
    name      = "gitlab-runner-repo-${var.argocd_project}"
    namespace = var.argocd_namespace
    labels = {
      "argocd.argoproj.io/secret-type" = "repository"
    }
  }
  data = {
    project = var.argocd_project
    type    = "git"
    url     = var.gitlab_runner_repo_url
  }
  type = "Opaque"
}

resource "time_sleep" "wait_10_seconds" {
  depends_on = [kubernetes_secret.argocd_repository]

  create_duration = "10s"
}

resource "kubernetes_manifest" "argocd_application" {
  manifest = yamldecode(
    templatefile("${path.module}/argocd-application.yaml", {
      name             = "gitlab-runner-${var.argocd_project}"
      argocd_namespace = var.argocd_namespace
      project          = var.argocd_project
      repo             = var.gitlab_runner_repo_url
      namespace        = var.gitlab_runner_namespace
      token            = var.gitlab_runner_token
      destination      = var.argocd_destination_cluster
    })
  )
  depends_on = [kubernetes_secret.argocd_repository, time_sleep.wait_10_seconds]
}
